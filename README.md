# Validation Center

## Description
This sub-project, that is a workgroup within SYLVA project, is intended to define the list of CNFs and derivative stacks to be validated (meaning demonstrating CNFs can work on top of SYLVA stack and derivative stacks icoporate all the capabilities included in SYLVA), build scripts to run test in order to validate use of SYLVA capabilities by CNFs, setup the validation platform(s), run the required tests to validate CNFs and derived stacks and keep a registry of validations performed.

Information about next meetings on this sub-project is available at https://gitlab.com/sylva-projects/validation_center/-/wikis/home

## Visuals
The following is an image of the CNF Validation process

![Image](./Pictures/CNF_Validation_Process_Workflow.png)

## Usage
The way of working of this subproject is intended to run as follows
* members to propose CNFs to be validated via comments to issue #1 in this sub-project 
* propose the candidate list of CNFs to TSC for approval
* for each CNF agreed to be validated assign a validation team
* each validation team defines the scripts, tools and tests required
* each validation team defines a validation timeplan
* each validation team executes the validation timeplan following the *validation strategy* (https://gitlab.com/sylva-projects/validation_center/-/blob/main/Validation%20program%20strategy.md)
* meet periodically every 2-to-3 weeks to review progress, assign tasks and define the list of candidate CNFs to be validated  
* keep a registry of validations performed and result
* keep a registry of scripts generated in order to allow reusability for future validation processes

## Project status
This project is in the initial steps, the validation process defined, the first validation platform has been setup and a dry-run of CNF validation process has been performed. 

The next steps expected are the building of the list of first CNFs to be validated (called the *CNF validation backlog*), the generation of the first validation scripts and the running of the validation process for the first CNFs in the backlog list.

Definition of the validation process for derivative stacks is left for a later stage of the sub-project
