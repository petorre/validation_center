# Validation Report for <CNF_NAME> <CNF_VERSION>

- Date: < DAY - MONTH IN LETTERS - YEAR >

- Validation Responsible:  < NAME OF LEADER OF VALIDATION TEAM >

- Validation Team: < NAMES OF MEMBERS OF VALIDATION TEAM, INCLUDING THE RESPONSIBLE >

- Result: < PASSED/FAILED >

- Comments: < add any comments required to explain limitations or considerations for the validation run, write None if no comments are provided >

## Validated Workload

- Provider of CNF:  < NAME OF PROVIDER >

- Name of CNF: < NAME OF CNF >

- Version of CNF: < vX.Y.Z, or not applicable >

- Platform capabilities required: < mark an X where applicable >
  
  * [ ] CPU pinning
  * [ ] HugePages
  * [ ] Multus CNI
  * [ ] SR-IOV
  * [ ] PTP
  * [x] Calico CNI
  * [ ] Flannel CNI
  * [ ] Cillium CNI
  * [ ] Whereabouts CNI
  * [ ] WeaveNet CNI

## Validation Platform

- Sylva version: < vX.Y.Z format >

- Validation environment used: < code of platform used for validation, e.g.: TEF-1 >

- Flavour used for validation: < mark an X where applicable >
  
  * [ ] Baremetal
  * [x] OpenStack
  * [ ] VMware stack
  * [ ] Vanilla k8s
  * [x] RKE2

- HW used for the platform (servers):

  - < LIST OF SERVERS (NUMBER, PROVIDER, MODEL, vCPUs, RAM and Local Storage) USED FOR THE VALIDATION PLATFORM >

## Validation Evidences

### Evidence of Platform Configuration

The following is a screenshot of the evidence that the platform was configured with the capabilities required by the workload under validation.

![image](assets/ValidationReportTemplate_ImageExample1.png)

### Evidence of Workload Deployment

The following is a screenshot of the evidence that the workload was successfully deployed in SYLVA

![image](assets/ValidationReportTemplate_ImageExample2.png)

### Evidence of Workload Basic Functional Testing 

#### Functional test 1

- Title: < NAME OF THE TEST >
- Description: < BRIEF DESCRIPTION >
- Results expected: < WHAT TO BE EXPECTED >
- Evidences: the following screenshot demonstrates that this functional test was successfully run and provides the expected output.

![image](assets/ValidationReportTemplate_ImageExample2.png)

< Repeat this for all the functional tests defined for this workload >

### Evidence of Workload making use of SYLVA capabilities

- Telco grade capability: < NAME OF CAPABILITY, e.g.: Multus CNI >
- Description: < BRIEF DESCRIPTION OF THE CAPABILITY AND THE TEST EXECUTED TO CHECK ITS USE >

< Repeat this for all the functional tests defined for this workload >
