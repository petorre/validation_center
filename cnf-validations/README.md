Under this directory, a working sub-directory is created to keep the artifacts related for each of the CNFs running the validation process. The format of each sub-directory must be <**name-of-CNF**>_ <**provider**>_ <**version**>.

The type of artifacts that can be stored for each CNF are the following (for instance)
1. Timeplan defined for the validation
2. High level archit Document describing the functionality under validation 
3. Scripts to run functional tests 
4. Evidence of validation executed / Results
