# Validation Report for Oracle PCF v1.11.0

- Date: **03 - July - 2023**

- Validation Responsible:  **Guillermo Carreto (Telefónica)**

- Validation Team: **Guillermo Carreto (Telefónica), Gerardo García (Telefónica) and Luis López (Oracle)**

- Result: **PASSED**

- Comments: **CNF has been deployed in Sylva by using ETSI's OpenSource MANO R12 (OSM R12) orchestrator.**

## Validated Workload

- Provider of CNF:  **Oracle**

- Name of CNF: **Policy Control Function (PCF)**

- Version of CNF: **v1.11.0**

- Platform capabilities required:
  
  * [ ] CPU pinning
  * [ ] HugePages
  * [ ] Multus CNI
  * [ ] SR-IOV
  * [ ] PTP
  * [x] Calico CNI
  * [ ] Flannel CNI
  * [ ] Cillium CNI
  * [ ] Whereabouts CNI
  * [ ] WeaveNet CNI

## Validation Platform

- Sylva version: **v0.1.2**

- Validation environment used: **TEF-1**

- Flavour used for validation:
  
  * [ ] Baremetal
  * [x] OpenStack
  * [ ] VMware stack
  * [ ] Vanilla k8s
  * [x] RKE2

- HW used for the platform (servers):

  - **4 x HPE ProLiant DL380 Gen10 (72 vCPUs, 384GB RAM, 878GB Local Storage)**
  - **2 x HPE ProLiant DL380 Gen10 (72 vCPUs, 576GB RAM, 731GB Local Storage)**
  - **2 x Intel R2208WFTZSR (96 vCPUs, 192GB RAM, 438GB Local Storage)**

## Validation Evidences

### Evidence of Platform Configuration

The following screenshot is the evidence that demonstrates that the platform has been successfully configured with the capabilities required by Oracle PCF v1.11.0, specified above.

![image](assets/evidence_platform_configuration_oracle_pcf_1.11.0.png)

### Evidence of Workload Deployment

The following screenshots are the evidence that demonstrate that Oracle PCF v1.11.0 has been deployed successfully in Sylva.

#### PCF's Monitoring Tier (Elastic Search, Jaeger, Kibana, Grafana and Prometheus)

![image](assets/evidence_occneinfra_pods_running_oracle_pcf_1.11.0.png){width=550px height=400px}

#### PCF's DataBase Tier (MySQL)

![image](assets/evidence_occnecndbtier_pods_running_oracle_pcf_1.11.0.png){width=500px height=120px}

#### PCF's Core Tier (CNF itself)

![image](assets/evidence_occnp_pods_running_oracle_pcf_1.11.0.png){width=500px height=300px}

### Evidence of Workload Basic Functional Testing 

#### Functional test 1

- Title: **Create 1000 sessions on PCF**
- Description: the main objective of this test is to create 1000 sessions on PCF by executing a script.
- Results expected: sessions must be registered in PCF DataBase Tier and represented in "Active sessions" graph in the Grafana dashboard that is part of the monitoring information reflected on PCF's Monitoring Tier.
- Evidences: the following screenshot demonstrates that this functional test was successfully run and provides the expected output.

![image](assets/evidence_grafana_active_sessions_oracle_pcf_1.11.0.png){width=1000px height=240px}

### Evidence of Workload making use of SYLVA capabilities

- Telco grade capability: **Calico CNI**
- Description: Calico CNI is required to secure network communication and manage network policies in the cluster for pods and hosts.

For this type of capability, a screenshot to demonstrate its use is not required.
