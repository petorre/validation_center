# Lessons learnt during validation cycles

1. **Ephemeral storage:** Pods in workload cluster use as ephemeral storage endpoint the disk partition mounted on  `/var/lib/kubelet`. Hence, it is important to:
   - Request vendors to provide an estimation of the ephemeral storage required by their CNFs.
   - Allow flexible sizing of `/var/lib/kubelet` disk partition on cluster nodes (local or volume based storage) in order to satisfy CNF's requirements on ephemeral storage.

2. **Persistent storage:** Using local storage of workload cluster nodes as backend of persistent storage to the PVs is inefficient, as it represents another factor to consider when sizing the disk partitions of each workload cluster node. A more efficient solution, when Sylva is installed over OpenStack, would be using `cinder-csi` storage class to deploy PVs as OpenStack volumes.
   - Sylva will support `cinder-csi` in workload clusters in `v0.2`. Please, click on the [LINK](https://gitlab.com/sylva-projects/sylva-core/-/issues/183) to know the progress on this issue in GitLab.

3. **Node selectors:** the CNF under validation should not include vendor-specific matching labels for selecting nodes to deploy their workloads. This is not a recommended practice and will be considered as undesired design feature as it implies vendor-specific modifications in the validation platform. As a minimum, the CNF's charts should allow customization of such requested labels, being the default option no label requirements.

4. **PV/PVCs size:** We have identified that the size requested for some PVCs is too big (up to ~115 GB in some cases) for a validation process. Thus, all that storage space is fully allocated but never fully used, since the CNFs are not been stressed at full rate. Considering this, it is important to:
   - Request vendors to keep the PVC requests to the minimal capacity for being functional for a validation process. This will allow a more efficient use of platform's storage.
