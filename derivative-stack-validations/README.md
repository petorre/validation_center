Under this directory, a working sub-directory is created to keep the artifacts related for each of the derivative stacks (distributions) running the validation process. 

The format of each sub-directory must be must be <**provider**>_ <**stack version**>.

The type of artifacts that can be stored for each CNF are the following (for instance)

1. Timeplan defined for the validation
2. High level archit Document describing the CNFs that will be used to test the stack under validation
3. Scripts to run tests to confirm the capabilities are incorporated in the derived stack under test
4. Evidence of validation executed / Results
