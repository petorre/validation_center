# Functest Xtesting - CNF Deployment tests

## Description

This section describes how to use [Functest Xtesting](https://github.com/opnfv/functest-xtesting) framework to launch a battery of tests against a Sylva's Validation Center cluster to validate that the kubernetes objects of the CNF under validation have been correctly deployed and are ready to implement their functionality. Currently, the test developed check, for every NAMESPACE passed as input, that:

- Pod have been correctly deployed, which means that all the pods are in RUNNING, COMPLETED or SUCCEEDED phase and RUNNING pods are in READY condition.

## Usage

**Functest Xtesting** allows to run a set of tests defined as a `Python` package contained in a Docker container. Considering it, to build your own Docker image and run it as a Docker container the following tasks must be executed.

1. **Clone this repository on a machine with the following features and tools available on it:**

  - Docker
  - API access to the Sylva's Validation Center cluster under analysis
  - Internet access

2. **Adapt the following files to your environment context:**

  - _kubeconfig.yaml:_ complete the information with the authentication data that you use to access the Sylva's Validation Center cluster under analysis.

3. **Build your own Docker image:** execute the following command in a terminal pointing to the directory where you have the cloned/adapted files inside your machine. It will build a new Docker image with the name _sylva_xtesting_deployment_.

```bash
docker build -t sylva_xtesting_deployment . # The name of the image is arbitrary
```

4. **Run a Docker container based on the image created:** execute the following command to run a container based on the previous image created, passing to it the required `ENV` variables for the execution. 

```bash
namespaces="default,cnf1,cnf2,cnf3" # Adapt this string to the list of namespaces that want to be tested.
kubeconfig=/home/ubuntu/kubeconfig.yaml # Adapt the value to the path where your kubeconfig file is stored. 

docker run --env NAMESPACES=$namespaces --env KUBECONFIG=$kubeconfig --name sylva_xtesting_t1 sylva_xtesting_deployment
```

> **NOTE:** In this case, `NAMESPACES` and `KUBECONFIG` are mandatory variables but additional environment variables could be used depending on your environment's context (e.g: if you access the cluster through a proxy server you may need to configure `HTTPS_PROXY`, `HTTP_PROXY`, `https_proxy`, `http_proxy`, `NO_PROXY` and `no_proxy` variables).
