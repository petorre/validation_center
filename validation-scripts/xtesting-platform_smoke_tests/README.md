# Functest Xtesting - Platform Smoke Tests

## Description

This section describes how to use [Functest Xtesting](https://github.com/opnfv/functest-xtesting) framework to launch a battery of smoke tests against a Sylva's k8s cluster to validate the installation of the platform telco features required by the CNF that will be validated over it. Currently, the platform telco features validated are:

- Multus CNI installation
- Multus NAD CRD creation
- SR-IOV CNI installation
- SR-IOV Device Plugin installation
- Whereabouts CNI installation
- Calico CNI installation
- Flannel CNI installation
- Cilium CNI installation
- WeaveNet CNI installation

## Usage

**Functest Xtesting** allows to run a set of tests defined as a `Python` package contained in a Docker container. Considering it, to build your own Docker image and run it as a Docker container the following tasks must be executed.

1. **Clone this repository on a machine with the following features and tools available on it:**

  - Docker
  - API access to the Sylva's k8s cluster under analysis
  - Internet access

2. **Adapt the following files to your environment context:**

  - _kubeconfig.yaml:_ complete the information with the authentication data that you use to access the Sylva's Validation Center k8s cluster under analysis.

3. **Build your own Docker image:** execute the following command in a terminal pointing to the folder where you have the cloned/adapted files inside your machine. It will build a new Docker image with the name _sylva_xtesting_.

```bash
docker build -t sylva_xtesting . # The name of the image is arbitrary
```

4. **Run a Docker container based on the image created:** execute the following command to run a container based on the previous image created, passing to it the required `ENV` variables for the execution.

```bash
kubeconfig=/src/kubeconfig.yaml # Adapt the value to the path where the kubeconfig file is stored within the container

docker run --env KUBECONFIG=$kubeconfig --name sylva_xtesting_t1 sylva_xtesting
```

  > **NOTE:** In this case, `KUBECONFIG` is a mandatory environment variable but additional environment variables could be used depending on your environment's context (e.g: if you access the cluster through a proxy server you may need to configure `HTTPS_PROXY`, `HTTP_PROXY`, `https_proxy`, `http_proxy`, `NO_PROXY` and `no_proxy` variables).