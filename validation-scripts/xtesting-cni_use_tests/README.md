# Functest Xtesting - CNI Use Tests

## Description

This section describes how to use [Functest Xtesting](https://github.com/opnfv/functest-xtesting) framework to launch a battery of tests against a Sylva's k8s cluster to validate the correct installation, configuration and use of the CNIs required by the CNF that will be validated over it. Currently, CNIs supported are:

- Multus CNI
- SR-IOV CNI

## Usage

**Functest Xtesting** allows to run a set of tests defined as a `Python` package contained in a Docker container. Considering it, to build your own Docker image and run it as a Docker container the following tasks must be executed.

1. **Clone this repository on a machine with the following features and tools available on it:**

  - Docker
  - API access to the Sylva's k8s cluster under analysis
  - Internet access

2. **Adapt the following files to your environment context:**

  - Check build.sh and run.sh for how optional KUBECONFIG, http_proxy and https_proxy environment variables are used.

3. **Build your own Docker image:** build.sh will build a new Docker image with the name _cni-use-tests_. In the folder where you have the cloned/adapted files, run it with

```bash
./build.sh
```

4. **Run a Docker container based on the image created:** run it with

```bash
./run.sh
```
